# BioModels on Jummp #
BioModels is being upgraded to a new infrastructure based on [Jummp](https://bitbucket.org/jummp/jummp/wiki/). 
It's been reused advanced features developed in Jummp like fully versioned storage engine with fine grained access right 
settings and supports multiple modelling formats.
## Improvements and New Features ##
* Improve streamlined submission process.
* Improve model display.
* Model can be downloaded as a COMBINE Archive.
* Support submission in multiple formats.
* Support for collaborative curation -- updates, sharing of private models, publishing, notifications and team working.
* Implement faceted search -- search results can be progressively filtered by curation status, model format, 
modelling approaches, disease, keywords, cross references (UniProt, ChEBI, Ensembl, etc.) 
* Search results can be sorted by name, authors, publication year and they are downloadable as an archive.
## User manuals ##
Supported by a contextual sensitive help system, users are able to jummp to the right assistance 
when trying to operate a specific function. This help system tries to locate you at the 
documentation page corresponding to the functionality you are discovering. To get the 
full online documentation, please take a look at  
[https://bitbucket.org/jummp/jummp/wiki/manuals](https://bitbucket.org/jummp/jummp/wiki/manuals)
## Install ##
See [https://bitbucket.org/jummp/jummp/wiki/install](https://bitbucket.org/jummp/jummp/wiki/install)
## Development ##
See [https://bitbucket.org/jummp/jummp/wiki/dev](https://bitbucket.org/jummp/jummp/wiki/dev)
## Developers ##
* [Mihai Glont](https://bitbucket.org/MihaiGlont/jummp-biomodels)
* [Tung Nguyen](https://bitbucket.org/biomodels/jummp-biomodels/)
## Contact ##
Email: [biomodels-developers@lists.sf.net](biomodels-developers@lists.sf.net)
## Licensing ##
BioModels source code is distributed under the GNU Affero General Public License. 
Please read [license.txt](license.txt) for information on the software availability and distribution.
