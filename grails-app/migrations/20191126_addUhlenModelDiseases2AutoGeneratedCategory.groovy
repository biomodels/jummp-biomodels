import grails.util.Holders
import net.biomodels.jummp.deployment.biomodels.AutoGeneratedCategory

databaseChangeLog = {
    changeSet(author: "tnguyen (customised)", id: "1574781701449-2") {
        grailsChange {
            change {
                File csvFile =
                    Holders.grailsApplication.mainContext.getResource("UhlenModelCategoryMapping.csv").file
                int i = 0
                csvFile.eachLine { String line ->
                    i += 1
                    def split = line.split(",")
                    String modelId = split[0]
                    String cateName = split[1]
                    String cleanCateName = cateName.substring(1, cateName.length() - 1)
                    AutoGeneratedCategory cate =
                        AutoGeneratedCategory.findOrSaveByModelIdentifierAndCategoryName(modelId, cleanCateName)
                    if (i%50 == 0) {
                        // clear session and save records after every 50 entries created
                        AutoGeneratedCategory.withSession { session ->
                            session.flush()
                            session.clear()
                        }
                    }
                }
                // clear session and save last records
                AutoGeneratedCategory.withSession { session ->
                    session.flush()
                    session.clear()
                }
            }
        }
    }
}
