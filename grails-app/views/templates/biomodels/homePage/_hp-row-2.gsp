<div id="hp-statistics-published-recently" class="large-4 medium-12 small-12 columns">
   <biomd:renderRecentlyPublishedModels/>
</div>

<div class="small-12 medium-16 large-4 columns">
    <biomd:renderRecentlyAccessedModels/>
</div>

<div class="small-12 medium-12 large-4 columns">
    <biomd:renderNewsWidget/>
</div>
