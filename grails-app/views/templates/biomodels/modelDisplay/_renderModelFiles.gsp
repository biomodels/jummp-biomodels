<tr>
    <td colspan="4">
        <h3 class="type-of-file-heading"><g:message code="submission.upload.mainFile.biomodels.label"/></h3>
    </td>
</tr>
<g:render template="/templates/biomodels/modelDisplay/renderFiles"/>
