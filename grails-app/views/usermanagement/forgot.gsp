<%--
 Copyright (C) 2010-2020 EMBL-European Bioinformatics Institute (EMBL-EBI),
 Deutsches Krebsforschungszentrum (DKFZ)

 This file is part of Jummp.

 Jummp is free software; you can redistribute it and/or modify it under the
 terms of the GNU Affero General Public License as published by the Free
 Software Foundation; either version 3 of the License, or (at your option) any
 later version.

 Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License along
 with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
--%>











<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="layout" content="${session['branding.style']}/main" />
        <title>${title}</title>
        <g:javascript>
            // define the variables tightened on the working user for later usages in common.js
            var currentUsername = "";
            var currentEmail = "";
            var currentRealName = "";
            var currentOrcid = "";
            var actionName = "${params.action}";
        </g:javascript>
    </head>
     <body>
        <div id="requestResetPassword" class="row">
            <div class="small-12 medium-6 medium-centered large-6 large-centered columns">
            <p><g:message code="user.forgot.ui.explanation"/></p>
            <g:form name="passwordForm" action="requestPassword" useToken="true">
                <div class="row column request-reset-password-form">
                    <label class="required"><g:message code="user.forgot.ui.username"/></label>
                    <g:textField name="username" placeholder="Enter your username"/>
                    <input type="submit" class="button" value="Submit"/>
                </div>
            </g:form>
            </div>
        </div>
    </body>
</html>
<content tag="title">
	<g:message code="user.forgot.ui.heading"/>
</content>
