<%--
 Copyright (C) 2010-2020 EMBL-European Bioinformatics Institute (EMBL-EBI),
 Deutsches Krebsforschungszentrum (DKFZ)

 This file is part of Jummp.

 Jummp is free software; you can redistribute it and/or modify it under the
 terms of the GNU Affero General Public License as published by the Free
 Software Foundation; either version 3 of the License, or (at your option) any
 later version.

 Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License along
 with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
--%>











<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="layout" content="${session['branding.style']}/main" />
        <title>${title}</title>
        <style>
        	.verysecure {
        		visibility:hidden;
        	}
            #announcementBox {
                background-color: yellow;
                border-style: solid;
                border-color: #ffcc00;
                border-width: 2px;
                padding: 10px 10px 0px 10px;
            }
        </style>
    </head>
    <body>
        <div class="row">
            <div class="columns small-12 medium-6 medium-centered large-6 large-centered">
                <div id="announcementBox">
                    <p>If you were already registered with us in
                    <a href="https://www.ebi.ac.uk/biomodels/content/news/retirement-party-for-the-classic-biomodels">the retired platform</a> and haven't logged in this new one yet,
                    please request a new password by
                clicking <a href="${grailsApplication.config.grails.serverURL}/forgotpassword">forgot password</a>
                        and entering your username.</p>
                    <p>If you prefer, you can create a new account with BioModels by completing the form below.</p>
                </div>
            </div>
        </div>
        <g:render template="/templates/initRegistration" plugin="jummp-plugin-web-application" />
        <div id="register" class="row">
            <div class="small-12 medium-6 medium-centered large-4 large-centered columns">
                <g:form name="registerForm" action="signUp" onkeypress="return event.keyCode != 13;" useToken="true">
                    <div class="row column register-form">
                        <g:render template="/templates/newAccountRegistrationForm"
                                  plugin="jummp-plugin-web-application" model="[user: null]" />
                        <label class="required" for="captcha"><g:message code="user.signup.ui.captcha"/></label>
                        <img style="margin-top:0;float:none" src="${createLink(controller: 'simpleCaptcha', action: 'captcha')}"/>
                        <br/>
                        <g:textField name="captcha" required="true"/>

                        <p><input type="submit" class="button" value="${g.message(code: 'user.signup.register')}"/>
                        <input type="reset" class="button" id="resetFormButton" value="${g.message(code: 'user.signup.reset')}"/>
                        </p>
                    </div>
                    <label class="verysecure">You shouldn't see me.</label>
                    <input class="verysecure" name="securityfeature" value=""/>
                </g:form>
            </div>
        </div>

        <script type="application/javascript" src="${resource(dir: 'js', file: 'common.js')}"></script>
    </body>
</html>
<content tag="title">
    <g:message code="user.signup.ui.heading.register"/>
</content>
