/**
 * Copyright (C) 2018 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 **/

package net.biomodels.jummp.deployment.biomodels

import grails.converters.JSON
import grails.converters.XML
import grails.plugin.springsecurity.annotation.Secured
import net.biomodels.jummp.deployment.biomodels.parameters.ParameterSearchCommand
import net.biomodels.jummp.deployment.biomodels.parameters.ParameterSearchResults
import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import grails.rest.*
import org.perf4j.aop.Profiled
import org.springframework.validation.FieldError

import java.nio.charset.StandardCharsets

/**
 * @author carankalle on 08/10/2018.
 */

@Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
@Resource(uri = '/parameterSearch')
class ParameterSearchController {
    def parameterSearchService
    /**
     * The class logger.
     */
    static final Log log = LogFactory.getLog(ParameterSearchController.class)

    final String IOExceptionCustomMessage = """\
Unable to retrieve data from EBI Search due to some problems with the request parameters, 
please use the suitable request parameters and try again."""

    @Profiled
    def index(ParameterSearchCommand command) {
        if (!command.validate()) {
            def msg = "Invalid request $command.query, ${command.errors.allErrors.inspect().toString()}"
            log.error(msg)
            return ["message": msg, "command": command]
        }
        response.setHeader("Access-Control-Allow-Origin", "https://ebi.emblstatic.net")
        response.setHeader("Vary", "Origin")
        render(view: "index", model: [command: command])
    }

    @Profiled
    def search(ParameterSearchCommand command) {
        String commandErrorMessage
        final def NoMatchesFoundMessage = "No matches found"
        String format = "xml"
        try {
            withFormat {
                json {
                    format = "json"
                    if (!validateCommandObject(command, format)) {

                        renderErrorMessage(commandErrorMessage, format, 400)
                        return
                    }
                    ParameterSearchResults resultJSON = parameterSearchService.getJSONData(command)
                    if (resultJSON.hasProperty('recordsTotal') && resultJSON['recordsTotal'] == 0) {
                        renderErrorMessage(NoMatchesFoundMessage, format, 200)
                        return
                    }
                    response.setContentType("application/json")
                    render(resultJSON as JSON)
                }
                xml {
                    format = "xml"
                    if (!validateCommandObject(command, format)) {
                        return
                    }
                    ParameterSearchResults resultJSON = parameterSearchService.getJSONData(command)
                    if (resultJSON.hasProperty('recordsTotal') && resultJSON['recordsTotal'] == 0) {
                        renderErrorMessage(NoMatchesFoundMessage, format, 200)
                        return
                    }
                    response.setContentType("text/xml")
                    render(resultJSON as XML)
                }
                csv {
                    format = "csv"
                    if (!validateCommandObject(command, format)) {
                        return
                    }
                    String resultCSV = parameterSearchService.getCSVData(command)
                    if (null == resultCSV || resultCSV.isEmpty()) {
                        renderErrorMessage(NoMatchesFoundMessage, format, 200)
                        return
                    }
                    response.setContentType("text/csv")
                    render(resultCSV, encoding: 'UTF-8')
                }
                '*' {
                    response.status = 415
                    render(['message': "Invalid format, please choose the format from JSON, XML and CSV"] as JSON)
                }
            }
        } catch (IllegalArgumentException ie) {
            log.error(ie.message, ie)
            renderErrorMessage(ie.getMessage(), format, 400)
        } catch (IOException ioe) {
            log.error(ioe.message, ioe)
            renderErrorMessage(IOExceptionCustomMessage, format, 400)
        } catch (Exception ex) {
            String msg = "Error encountered while processing $command, No matches found"
            log.error(ex.message, ex)
            renderErrorMessage(msg, format, 500)
        }
    }

    @Profiled
    def export(ParameterSearchCommand command) {
        String format = "csv"
        if (!validateCommandObject(command, format)) {
            return
        }
        try {
            String resultCSV = parameterSearchService.exportData(command)
            if (null == resultCSV || resultCSV.isEmpty()) {
                final String NoMatchesFoundMessage = "No matches found"
                renderErrorMessage(NoMatchesFoundMessage, format, 200)
                return
            }
            String filename = "BioModels_Parameters_Export-${new Date().format("yyyy-MM-dd")}.csv"
            response.setContentType("text/csv; header=present; charset=UTF-8")
            response.setHeader("Content-Disposition", "attachment;filename=${filename}")
            def content = resultCSV.getBytes(StandardCharsets.UTF_8)
            response.outputStream << new ByteArrayInputStream(content)
        } catch (IllegalArgumentException ie) {
            log.error(ie.message, ie)
            renderErrorMessage(ie.getMessage(), format, 400)
        } catch (IOException ioe) {
            log.error(ioe.message, ioe)
            renderErrorMessage(ioe.message, format, 400)

        } catch (Exception ex) {
            String msg = "Error encountered while processing $command, No matches found"
            log.error(ex.message, ex)
            renderErrorMessage(msg, format, 500)
        }
    }

    private static prepareJsonErrorMessage(String message) {
        def errorObject = ['recordsTotal': 0, 'recordsFiltered': 0, 'entries': []]
        errorObject['message'] = message
        return errorObject
    }

    @Profiled
    private boolean validateCommandObject(ParameterSearchCommand command, String format) {
        if (!command.validate()) {
            response.status = 400
            String errorString = parseErrors(command.errors.fieldErrors)
            String commandErrorMessage = "Invalid request parameter. $errorString"
            log.error(commandErrorMessage)
            renderErrorMessage(commandErrorMessage, format, 400)
            return false
        }
        return true
    }

    @Profiled
    private void renderErrorMessage(String msg, String format, int statusCode) {
        response.status = statusCode
        if (format == "json") {
            render(prepareJsonErrorMessage(msg) as JSON)
        } else if (format == "xml") {
            def responseContent = "<errors><message>${msg}</message></errors>"
            response.setContentType("text/xml")
            render(responseContent)
        } else if (format == "csv") {
            response.setContentType("text/plain")
            render(msg)
        }
    }

    @Profiled
    private static String parseErrors(List<FieldError> fieldErrors) {
        String errorString = ""
        for (int i = 0; i < fieldErrors.size(); i++) {
            String rejectedField = fieldErrors.get(i).field + "=" + fieldErrors.get(i).rejectedValue
            if (!errorString.isEmpty()) {
                errorString += ", " + rejectedField
            } else {
                errorString = "Rejected parameters: " + rejectedField
            }
        }
        return errorString
    }
}

