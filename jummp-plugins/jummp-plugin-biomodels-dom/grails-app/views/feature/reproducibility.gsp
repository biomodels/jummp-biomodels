<%--
  Created by IntelliJ IDEA.
  User: nvntung@gmail.com
  Date: 07/12/2020
  Time: 22:44
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="layout" content="${grailsApplication.config.jummp.branding.style}/main"/>
    <title>Reproducibility in Systems Biology Modelling | BioModels</title>
</head>

<body>
${content}
</body>
</html>
