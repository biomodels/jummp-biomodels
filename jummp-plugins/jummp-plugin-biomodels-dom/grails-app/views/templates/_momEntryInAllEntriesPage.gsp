<li>
    <strong>${entry.monthName}</strong>: ${entry.links}
    <br/>
    <span>
        <a href="${entry.momLink}" target="_blank">${entry.title}</a>
    </span>
    <br/>
    <em>${entry.authors}</em>
</li>
