<div class="row">
    <div class="small-12 medium-12 large-12 columns">
        <h3><a name="${cate.key}">${cate.key}</a>&nbsp;<span style="font-size: large">(${cate?.value?.size()})</span>
            <span style="font-size: small; cursor: pointer" class="button float-right back-to-top">Back to top</span></h3>
    </div>
</div>
