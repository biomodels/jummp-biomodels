<%--
 Copyright (C) 2010-2014 EMBL-European Bioinformatics Institute (EMBL-EBI),
 Deutsches Krebsforschungszentrum (DKFZ)

 This file is part of Jummp.

 Jummp is free software; you can redistribute it and/or modify it under the
 terms of the GNU Affero General Public License as published by the Free
 Software Foundation; either version 3 of the License, or (at your option) any
 later version.

 Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License along
 with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
--%>



<style>
body {
    background: #f5f5f5;
}

.classifiers {
    list-style: none;
    margin: 0;
    text-shadow: 0 0 1px rgba(255, 255, 255, 0.004);
    font-size: 100%;
    font-weight: 400;
    max-width: 1200px;
    padding: 10px 0 30px 0;
    box-sizing: border-box;
}

.classifier-item {
    display: block;
    margin-bottom: 10px;
    padding: 20px 0;
    border-radius: 2px;
    background: white;
    box-shadow: 0 2px 1px rgba(170, 170, 170, 0.25);
}

.classifier-name {
    font-weight: 400;
}

.list .classifier-item {
    position: relative;
    padding: 0;
    font-size: 14px;
    line-height: 50px;
}
.list .classifier-item .pull-right {
    position: absolute;
    right: 0;
    top: 0;
}
@media screen and (max-width: 800px) {
    .list .classifier-item .stage:not(.active) {
        display: none;
    }
}
@media screen and (max-width: 700px) {
    .list .classifier-item .classifier-progress-bg {
        display: none;
    }
}
@media screen and (max-width: 600px) {
    .list .classifier-item .pull-right {
        position: static;
        line-height: 20px;
        padding-bottom: 10px;
    }
}
.list .classifier-country,
.list .classifier-progress,
.list .classifier-completes,
.list .classifier-end-date {
    color: #A1A1A4;
}
.list .classifier-country,
.list .classifier-completes,
.list .classifier-end-date,
.list .classifier-name,
.list .classifier-stage {
    margin: 0 10px;
}
.list .classifier-country {
    margin-right: 0;
}
.list .classifier-end-date,
.list .classifier-completes,
.list .classifier-country,
.list .classifier-name {
    vertical-align: middle;
}
.list .classifier-end-date {
    display: inline-block;
    width: 145px;
    white-space: nowrap;
    overflow: hidden;
}

.classifier-stage .stage {
    display: inline-block;
    vertical-align: middle;
    width: 16px;
    height: 16px;
    overflow: hidden;
    border-radius: 50%;
    padding: 0;
    margin: 0 2px;
    background: #f2f2f2;
    text-indent: -9999px;
    color: transparent;
    line-height: 16px;
}
.classifier-stage .stage.active {
    background: #A1A1A4;
}

.list .list-only {
    display: auto;
}

.classifier-progress-label {
    vertical-align: middle;
    margin: 0 10px;
    color: #8DC63F;
}

.classifier-progress-labels span {
    display: inline-block;
    width: 70px;
}

.classifier-progress-bg {
    display: inline-block;
    vertical-align: middle;
    position: relative;
    width: 100px;
    height: 4px;
    border-radius: 2px;
    overflow: hidden;
    background: #eee;
}

.classifier-progress-fg {
    position: absolute;
    top: 0;
    bottom: 0;
    height: 100%;
    left: 0;
    margin: 0;
    background: #8DC63F;
}

.pretty.p-switch .state::before {
    top: 4px !important;
}

.pretty .state label::after, .pretty .state label::before {
    top: 5px !important;
}
.pretty .state label{
    color: #A1A1A4 !important;
}
.pretty.p-switch .state label {
    text-indent: 3em !important;
}
.x-close {
    position: relative;
    display: inline-block;
    width: 30px;
    height: 30px;
}
.x-close:hover::before, .x-close:hover::after {
    background: #1ebcc5;
}
.x-close::before, .x-close::after {
    content: '';
    position: absolute;
    height: 2px;
    width: 75%;
    top: 88%;
    right: 10px;
    margin-top: -1px;
    background: #A1A1A4;
}
.x-close::before {
    -webkit-transform: rotate(45deg);
    -moz-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    -o-transform: rotate(45deg);
    transform: rotate(45deg);
}
.x-close::after {
    -webkit-transform: rotate(-45deg);
    -moz-transform: rotate(-45deg);
    -ms-transform: rotate(-45deg);
    -o-transform: rotate(-45deg);
    transform: rotate(-45deg);
}
.x-close.hairline::before, .x-close.hairline::after {
    height: 1px;
}
.delete-deep-model {
    cursor: pointer;
}

.rebuild-progress {
    position: relative;
    height: 4px;
    display: inline-block;
    width: 55%;
    background-color: #eee;
    border-radius: 2px;
    background-clip: padding-box;
    margin: auto 0px auto 10px;
    overflow: hidden; }
.rebuild-progress .determinate {
    position: absolute;
    top: 0;
    bottom: 0;
    background-color: #5cb85c;
    transition: width .3s linear; }

.btn {
    position: relative;

    display: inline-block;
    padding: 0;

    overflow: hidden;

    border-width: 0;
    outline: none;
    border-radius: 2px;

    background-color: #2ecc71;
    color: #ecf0f1;

    margin: auto 20px auto 25px;

    cursor: pointer;

    transition: background-color .3s;
}

.btn:hover, .btn:focus {
    background-color: #27ae60;
}

.btn > * {
    position: relative;
}

.btn span {
    display: block;
    padding: 10px;
}

.btn:before {
    content: "";

    position: absolute;
    top: 50%;
    left: 50%;

    display: inline-block;
    width: 0;
    padding-top: 0;

    border-radius: 100%;

    background-color: rgba(236, 240, 241, .3);

    -webkit-transform: translate(-50%, -50%);
    -moz-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    -o-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
}

.btn:active:before {
    width: 120%;
    padding-top: 120%;

    transition: width .2s ease-out, padding-top .2s ease-out;
}

</style>

<ul class="classifiers list">
    <li class="classifier-item">

        <span class="classifier-country list-only">
            Rebuild status
        </span>

        <span>
            <span class="rebuild-progress">
                <span class="determinate" id="rebuild-progress-val" style="width: 70%"></span>
            </span>
            <span class="classifier-progress-label" id="rebuild-val">
                80%
            </span>
        </span>

        <div class="pull-right">
            <span class="classifier-end-date ended" id="rebuild-date">

            </span>
            <span>
                <button class="btn" type="button" id="rebuild-now">
                    <span>Rebuild now</span>
                </button>
            </span>
        </div>
    </li>
    <g:each in="${dlModels}">
        <g:set var="accuracy" value="${Math.round(Float.parseFloat(it['accuracy']) * 100)}"/>
        <g:set var="percent" value="${Math.round(Float.parseFloat(it['percent']))}"/>

        <li class="classifier-item">

            <span class="classifier-country list-only">
                ${message(code: 'modelclassifier.dllmodel.details.name')}
            </span>

            <span class="classifier-name" onclick="viewModel('${it['name']}')">
                <a> ${it['name']} </a>
            </span>

            <div class="pull-right">

                <span class="classifier-progress" onclick="viewModel('${it['name']}')">
                    <span class="classifier-progress-bg">
                        <span class="classifier-progress-fg" style="width: ${accuracy}%;"></span>
                    </span>

                    <span class="classifier-progress-labels" onclick="viewModel('${it['name']}')">
                        <span class="classifier-progress-label">
                            ${accuracy}%
                        </span>

                        <span class="classifier-completes">
                            ${percent} / 100
                        </span>
                    </span>
                </span>

                <span class="classifier-end-date ended" onclick="viewModel('${it['name']}')">
                    ${it['create_date']}
                </span>
                <span class="classifier-stage">
                    <div class="pretty p-switch">
                        <g:if test="${it['default'] == "true"}">
                            <input type="radio" name="activated" value="${it['name']}" checked />
                        </g:if>
                        <g:else>
                            <input type="radio" name="activated" value="${it['name']}" />
                        </g:else>
                        <div class="state p-success">
                            <label>${message(code: 'modelclassifier.dllmodel.switch.active')}</label>
                        </div>
                    </div>
                </span>
                <span class="delete-deep-model"
                      onclick="deleteModel('${it['name']}', event, '${message(code: 'modelclassifier.dllmodel.delete.confirm', args: [it['name']])}')">
                    <div class="x-close hairline"></div>
                </span>
            </div>
        </li>
    </g:each>
</ul>

<g:javascript>
    function viewModel(modelName) {
        window.location = $.jummp.createLink("classifierConfigure", "classifierDetails?model_name=" + encodeURIComponent(modelName))
    }

    function deleteModel(modelName, event, confirm_message) {
        if (!confirm(confirm_message)) {
            return;
        }
        var currentElement = $(event.target);
        $.ajax({
            dataType:'json',
            type: "DELETE",
            url: $.jummp.createLink("classifierConfigure", "delete?model_name=" + encodeURIComponent(modelName)),
            error: function(jqXHR) {
                toastr.error(jqXHR.responseJSON.message);
            },
            success: function() {
                toastr.success("${message(code: 'modelclassifier.dllmodel.delete.success')}");
                currentElement.parents(".classifier-item").remove()
            }
        });
    }

    function getRebuildStatus() {
        $.ajax({
            dataType:'json',
            type: "GET",
            url: $.jummp.createLink("classifierConfigure", "rebuildCacheStatus?time=" + Date.now()),
            error: function(jqXHR) {
                toastr.error(jqXHR.responseJSON.message);
            },
            success: function(data) {
                $("#rebuild-date").html(data.createDateStr);
                $("#rebuild-progress-val").css("width", data.percent + "%");
                $("#rebuild-val").html(data.percent + "%");
                setTimeout(getRebuildStatus, 3000);
            }
        });
    }

    $(document).ready(function () {
        getRebuildStatus();
        $('#configurationForm').submit(function (e) {
            e.preventDefault();
            if (!confirm("${message(code: 'modelclassifier.dllmodel.switch')}")) {
                return;
            }
            $.ajax({
                dataType:'json',
                type: "POST",
                url: $.jummp.createLink("classifierConfigure", "switchModel"),
                data: $(this).serialize(),
                error: function(jqXHR) {
                    toastr.error(jqXHR.responseJSON.message);
                },
                success: function(data) {
                    toastr.success("${message(code: 'modelclassifier.dllmodel.switch.success')}");
                }
            });
        });

        $("#createButton").click(function () {
            window.location = $.jummp.createLink("classifierConfigure", "classifierCreator")
        });

        $("#rebuild-now").click(function () {
            if (!confirm("${message(code: 'modelclassifier.cache.rebuild.confirm')}")) {
                return;
            }
            $.ajax({
                dataType:'json',
                type: "POST",
                url: $.jummp.createLink("classifierConfigure", "rebuildCache"),
                data: $(this).serialize(),
                error: function(jqXHR) {
                    toastr.error(jqXHR.responseJSON.message);
                },
                success: function(data) {
                    toastr.success("${message(code: 'modelclassifier.cache.rebuild.started')}");
                }
            });
        })
    });
</g:javascript>
