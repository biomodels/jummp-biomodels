/**
 * Copyright (C) 2010-2016 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.biomodels.jummp.deployment.biomodels

import net.biomodels.jummp.core.model.ModelTransportCommand
import net.biomodels.jummp.plugins.security.User

/**
 * @short   Data transfer object (DTO) for CurationNotes domain class.
 *
 * @author  Tung Nguyen <tung.nguyen@ebi.ac.uk>
 */
@grails.validation.Validateable
class CurationNotesTransportCommand implements Serializable {
    private static final long serialVersionUID = 1L
    private static final long MAX_IMG_SIZE =  1_500_000 // bytes ~ 1.44MB = 1024*1024*1.44

    Long id
    ModelTransportCommand model
    User submitter
    User lastModifier
    Date dateAdded
    Date lastModified
    String comment
    String internalComment
    byte[] curationImage
    String mimeType
    boolean updated

    static constraints = {
        importFrom(CurationNotes)
        id nullable: true
        mimeType nullable: true
        curationImage validator: { byte[] val, CurationNotesTransportCommand command ->
            String curationImgBase64Str
            curationImgBase64Str = val ? Base64.encoder.encodeToString(val) : null
            if (curationImgBase64Str == null) {
                return ['curationImageMissing']
            } else {
                def mimeTypePattern = /^image\//
                def m = command.mimeType =~ mimeTypePattern
                if (m.count <= 0) {
                    return ['curationNotesTransportCommand.curationImage.curationImageWrongFileType']
                }
            }
        }
    }
}
