package net.biomodels.jummp.deployment.biomodels.parameters

import groovy.transform.CompileStatic
import groovy.transform.ToString

/**
 * Created by carankalle on 31/10/2018.
 */
@CompileStatic
@ToString
class SearchResultEntry {
    Map<String, String> fields
}
