package net.biomodels.jummp.models

enum ModelClassStatus {
    UNTRAINED, TRAINED
}
