/**
 * Copyright (C) 2010-2019 EMBL-European Bioinformatics Institute (EMBL-EBI),
 * Deutsches Krebsforschungszentrum (DKFZ)
 *
 * This file is part of Jummp.
 *
 * Jummp is free software; you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
 */

package net.biomodels.jummp.deployment.biomodels

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 *
 * @testers     Tung Nguyen <tnguyen@ebi.ac.uk>
 */
@TestFor(P2mService)
@Mock([P2MMapping, AutoGeneratedCategory])
class P2mServiceSpec extends Specification {

    def setup() {
        P2MMapping.findOrSaveByRepresentativeAndMember("BMID0001", "BMID0021")
        P2MMapping.findOrSaveByRepresentativeAndMember("BMID0001", "BMID0022")
        P2MMapping.findOrSaveByRepresentativeAndMember("BMID0002", "BMID0030")
        P2MMapping.findOrSaveByRepresentativeAndMember("BMID0002", "BMID0031")
        P2MMapping.findOrSaveByRepresentativeAndMember("BMID0002", "BMID0032")
    }

    void "test getMissing() method"() {
        expect: "the method will return a non-empty list because data has been populated"
        service.findMissing().size()
    }

    void "test getRepresentativeId() method"() {
        given: "let's give a model identifier"
        String missingId = "BMID0021"
        when: "invoke the method"
        String repId = service.getRepresentativeId(missingId)
        then: "the representative identifier should be existent"
        repId
        when: "invoke the method with a non-existent identifier"
        missingId = "BMID0040"
        repId = service.getRepresentativeId(missingId)
        then: "the method should return a null value"
        !repId
    }

    void "test getRepresentatives() method"() {
        given: "a list of missing identifiers"
        List missingIds = ["BMID0002", "BMID0021", "BMID0022", "BMID0032", "BMID0040"]
        when: "invoking the method with the list of identifiers"
        Map representatives = service.getRepresentatives(missingIds)
        then: "the result should be a non-empty list"
        5 == representatives?.size()
        when: "ignore the models having no representatives"
        List nonNullValues = representatives.values().findAll { it != null }
        then: "the result should be equal to 4 because BMID0040 does not have any representative"
        4 == nonNullValues?.size()
    }

    void "test getModelCategoryMap() method"() {
        setup: "populate some records"
        String mId = "BMID000000141044"
        String cName = "Acetobacter"
        AutoGeneratedCategory.findOrSaveByModelIdentifierAndCategoryName(mId, cName).save(flush: true)
        mId = "BMID000000141191"
        cName = 'Acidaminococcus'
        AutoGeneratedCategory.findOrSaveByModelIdentifierAndCategoryName(mId, cName).save(flush: true)
        mId = "BMID000000141432"
        cName = 'Cyanidioschyzon'
        AutoGeneratedCategory.findOrSaveByModelIdentifierAndCategoryName(mId, cName).save(flush: true)
        mId = "BMID000000140863"
        cName = 'Dichelobacter'
        AutoGeneratedCategory.findOrSaveByModelIdentifierAndCategoryName(mId, cName).save(flush: true)


        when: "the method is called"
        Map result = service.getModelCategoryMap("BMID")

        then: "the method returns a map of four elements"
        3 == result.size()
        ["A", "C", "D"] as Set == result.keySet()
        ["Acetobacter", "Acidaminococcus"] as Set == result.get("A").collect { it.categoryName } as Set
    }

    void 'test getCategoryForModel'() {
        given: "an AutoGeneratedCategory for a representative P2M"
        String model = "BMID000000141044"
        String category = "Acetobacter"
        def agc = new AutoGeneratedCategory(modelIdentifier: model, categoryName: category)
        assert agc.save(flush: true)

        when: 'we look up the category for that model'
        String actualCategory = service.getCategoryForModel model

        then: 'it is found'
        category == actualCategory

        and: 'no category is shown for a model that does not exist'
        !service.getCategoryForModel('NOT-A-MODEL-ID')
        !service.getCategoryForModel(null)
    }
}
