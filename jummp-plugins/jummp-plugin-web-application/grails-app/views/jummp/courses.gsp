<%--
 Copyright (C) 2010-2017 EMBL-European Bioinformatics Institute (EMBL-EBI),
 Deutsches Krebsforschungszentrum (DKFZ)

 This file is part of Jummp.

 Jummp is free software; you can redistribute it and/or modify it under the
 terms of the GNU Affero General Public License as published by the Free
 Software Foundation; either version 3 of the License, or (at your option) any
 later version.

 Jummp is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

 You should have received a copy of the GNU Affero General Public License along
 with Jummp; if not, see <http://www.gnu.org/licenses/agpl-3.0.html>.
--%>






<head>
    <meta name="layout" content="${session['branding.style']}/main" />
    <style type="text/css">
        dt {
            display: block;
        }
        ul, ol, dl {
            line-height: 1.0;
        }
    </style>
</head>
<body>
<h2>Courses material</h2>

Here are the materials from some of the presentations, courses and tutorials which were provided by the BioModels.net team.

<h3 id="year_2017">2017</h3>
<ul class="news_list">
    <li id="item_20170712">
        <dl>
            <dt class="news-date">12<sup>th</sup> July 2017</dt>
            <dd>Event:
                <a href="http://www.ebi.ac.uk/training/events/2017/silico-systems-biology-0"
                   title="Website of the event">Joint EMBL-EBI-Wellcome Trust Course: In silico Systems Biology </a></dd>
            <dd>Topic: Tutorial on modelling signalling pathways using COPASI</dd>
            <dd>Materials:
                <a href="http://www.ebi.ac.uk/biomodels/courses/20170712/"
                   title="Access to all materials from this course">dedicated tutorial page</a></dd>
        </dl>
    </li>
</ul>

<h3 id="year_2016">2016</h3>
<ul class="news_list">
    <li id="item_20160706">
        <dl>
            <dt class="news-date">06<sup>th</sup> July 2016</dt>
            <dd>Event:
                <a href="http://www.ebi.ac.uk/training/events/2016/silico-systems-biology"
                   title="Website of the event">Joint EMBL-EBI-Wellcome Trust Course: In silico Systems Biology </a></dd>
            <dd>Topic: Tutorial on modelling signalling pathways using COPASI</dd>
            <dd>Materials:
                <a href="http://www.ebi.ac.uk/biomodels/courses/20160706/"
                   title="Access to all materials from this course">dedicated tutorial page</a></dd>
        </dl>
    </li>
</ul>

<h3 id="year_2015">2015</h3>
<ul class="news_list">
    <li id="item_20150610">
        <dl>
            <dt class="news-date">10<sup>th</sup> June 2015</dt>
            <dd>Event:
                <a href="http://www.wellcome.ac.uk/Education-resources/Courses-and-conferences/Advanced-Courses-and-Scientific-Conferences/Advanced-Courses/WTVM053676.htm#"
                   title="Website of the event">Joint EMBL-EBI-Wellcome Trust Course: In silico Systems Biology </a></dd>
            <dd>Topic: Tutorial on modelling signalling pathways using COPASI</dd>
            <dd>Materials:
                <a href="http://www.ebi.ac.uk/biomodels/courses/20150610/"
                   title="Access to all materials from this course">dedicated tutorial page</a></dd>
        </dl>
    </li>
</ul>

<h3 id="year_2013">2013</h3>
<ul class="news_list">
    <li id="item_20130904">
        <dl>
            <dt class="news-date">4<sup>th</sup> September 2013</dt>
            <dd>Event:
                <a href="http://www.icsb2013.dk/"
                   title="International Conference on Systems Biology (ICSB)">ICSB 2013</a> Workshop</dd>
            <dd>Topic:
                <a href="http://co.mbine.org/events/ICSB_2013"
                   title="Workshop page">Modelling and Simulation of Quantitative Biological Models</a></dd>
            <dd>Materials:
                <a href="http://www.ebi.ac.uk/biomodels/courses/20130904/biomodels-tutorial_20130904.pdf"
                   title="PDF slides: tutorial on BioModels Database">PDF slides of the tutorial</a> (3.4MB)</dd>
        </dl>
    </li>
    <li id="item_20130627">
        <dl>
            <dt class="news-date">25<sup>th</sup>-29<sup>th</sup> June 2013</dt>
            <dd>Event:
                <a href="http://www.wellcome.ac.uk/Education-resources/Courses-and-conferences/Advanced-Courses-and-Scientific-Conferences/Advanced-Courses/WTVM053676.htm"
                   title="Website of the event">Joint EMBL-EBI-Wellcome Trust Course: In silico Systems Biology </a></dd>
            <dd>Topic: Tutorial on modelling signalling pathways using COPASI</dd>
            <dd>Materials:
                <a href="http://www.ebi.ac.uk/biomodels/courses/20130627/"
                   title="Access to all materials from this course">dedicated tutorial page</a></dd>
            <dd>Topic: Lecture on BioModels Database</dd>
            <dd>Materials:
                <a href="http://www.ebi.ac.uk/biomodels/courses/20130627/biomdtalk_20130629.pdf"
                   title="PDF slides: Lecture on BioModels Database">PDF slides of the lecture</a></dd>
        </dl>
    </li>
</ul>

<h3 id="year_2012">2012</h3>
<ul class="news_list">
    <li id="item_20120427">
        <dl>
            <dt class="news-date">27<sup>th</sup> April 2012</dt>
            <dd>Event:
                <a href="http://www.wellcome.ac.uk/Education-resources/Courses-and-conferences/Advanced-Courses-and-Scientific-Conferences/Advanced-Courses/WTVM053676.htm"
                   title="Website of the event">Joint EMBL-EBI-Wellcome Trust Course: In silico Systems Biology </a></dd>
            <dd>Topic: tutorial about modelling signalling pathways and lecture on BioModels Database</dd>
            <dd>Materials:
                <a href="http://www.ebi.ac.uk/biomodels/courses/20120427/"
                   title="Access to all materials from this course">dedicated tutorial page</a>,
                <a href="http://www.ebi.ac.uk/biomodels/courses/20120427/biomdtalk_20120427.pdf"
                   title="PDF slides: Lecture on BioModels Database">PDF slides of the lecture</a> (3.9MB)
            </dd>
        </dl>
    </li>
</ul>

<h3 id="year_2011">2011</h3>
<ul class="news_list">
    <li id="item_20110413">
        <dl>
            <dt class="news-date">13<sup>th</sup> April 2011</dt>
            <dd>Event:
                <a href="http://dev.enfin.org/page.php?page=esb"
                   title="Website of the event">Conference - Enabling Systems Biology</a></dd>
            <dd>Topic: lecture about BioModels Database</dd>
            <dd>Materials:
                <a href="http://www.ebi.ac.uk/biomodels/courses//20110413/biomdtalk_13April2011.pdf"
                   title="PDF Slides of the lecture">PDF slides</a> (10.4MB)</dd>
        </dl>
    </li>
    <li id="item_20110304">
        <dl>
            <dt class="news-date">4<sup>th</sup> March 2011</dt>
            <dd>Event: Systems Biology Module, MSc Applied Bioinformatics, Cranfield University</dd>
            <dd>Topic: lecture about BioModels Database and tutorial about modelling signalling pathways</dd>
            <dd>Materials:
                <a href="http://www.ebi.ac.uk/biomodels/courses/20110304/"
                   title="Access to all materials from this course">dedicated tutorial page</a></dd>
        </dl>
    </li>
</ul>

<h3 id="year_2010">2010</h3>
<ul class="news_list">
    <li id="item_20101010">
        <dl>
            <dt class="news-date">10<sup>th</sup> October 2010</dt>
            <dd>Event:
                <a href="http://www.icsb2010.org/"
                   title="Website of the event">The 11th International Conference on Systems Biology (ICSB)</a></dd>
            <dd>Topic: introduction to BioModels Database and tutorial about modelling signalling pathways</dd>
            <dd>Materials:
                <a href="http://www.ebi.ac.uk/biomodels/courses/20101010/"
                   title="Access to all materials from this course">dedicated tutorial page</a></dd>
        </dl>
    </li>
    <li id="item_20100413">
        <dl>
            <dt class="news-date">13<sup>th</sup> April 2010</dt>
            <dd>Event:
                <a href="http://www.ebi.ac.uk/training/handson/course_100410_sysbiol_programme.html"
                   title="Website of the event">EMBO Practical Course 'In silico systems biology:
                network reconstruction, analysis and network based modelling'</a></dd>
            <dd>Topic: introduction to BioModels Database and tutorial about modelling signalling pathways using COPASI</dd>
            <dd>Materials:
                <a href="http://www.ebi.ac.uk/biomodels/courses/20100413/"
                   title="Access to all materials from this course">dedicated tutorial page</a></dd>
        </dl>
    </li>
</ul>

</body>
<content tag="courses">
    selected
</content>
<content tag="title">
    <g:message code="${titleCode}" default="Courses material" />
</content>
