<%--
  Created by IntelliJ IDEA.
  User: tnguyen@ebi.ac.uk
  Date: 02/09/2020
  Time: 15:22
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="${session['branding.style']}/main" />
    <title><g:message code="${titleCode}" default="Curator's Zone | BioModels"/></title>
</head>

<body>
<h2>Documentation for BioModels' Curators</h2>
    <a name="general-introduction-annotation"></a>
    <h3>General introduction to the annotation of models</h3>
    <p>Read more from the document: <a
        href="https://drive.google.com/file/d/1JqjcH0T0UTWMuBj-scIMwsyt2z38A0vp/view?usp=sharing">
        https://drive.google.com/file/d/1JqjcH0T0UTWMuBj-scIMwsyt2z38A0vp/view</a></p>
</body>
</html>
