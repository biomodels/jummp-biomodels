<h3>
    Found: ${totalCount} ${totalCount > 1 ? 'models' : 'model'}
    <g:if test="${action == 'search'}">
        <span style="float: right"><a id="btnDownload">Download</a></span>
        <span style="float: right"><a id="checkAll">Select all</a> |&nbsp;</span>
    </g:if>
</h3>
