<g:if test="${method == 'runFacetSearch'}">
    ${fv.label} (${fv.count})
</g:if>
<g:else>
    ${fv.label}
</g:else>
