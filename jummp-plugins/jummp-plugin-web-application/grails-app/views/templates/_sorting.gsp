<label style="display: inline-block; float: left; padding-right: 4px; width: 100%">Sort by
    <select name="sortBy"
            style="display: inline-block; width: 50%; font-size: 85%;
            height: 35px !important; margin: 0 0 0.125em;">
        <option value="relevance-desc">Relevance</option>
        <option value="first_author-asc">Author name: A to Z</option>
        <option value="first_author-desc">Author name: Z to A</option>
        <option value="id-asc">BioModels ID: A to Z</option>
        <option value="id-desc">BioModels ID: Z to A</option>
        <option value="name-asc">Model Name: A to Z</option>
        <option value="name-desc">Model Name: Z to A</option>
        <option value="publication_year-asc">Publication year: oldest first</option>
        <option value="publication_year-desc">Publication year: newest first</option>
    </select>
</label>
