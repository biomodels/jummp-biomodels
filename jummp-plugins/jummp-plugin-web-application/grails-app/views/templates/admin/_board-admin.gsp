<g:render template="/templates/admin/wcm-admin" plugin="jummp-plugin-web-application"/>
<g:render template="/templates/admin/indices-regeneration" plugin="jummp-plugin-web-application"/>
<g:render template="/templates/admin/omicsdi-export-settings" plugin="jummp-plugin-web-application"/>
<g:render template="/templates/admin/publication-management" plugin="jummp-plugin-web-application"/>
<g:render template="/templates/admin/redis-cache-management" plugin="jummp-plugin-web-application"/>
<g:render template="/templates/admin/system-configuration" plugin="jummp-plugin-web-application"/>
<g:render template="/templates/admin/user-management" plugin="jummp-plugin-web-application"/>
