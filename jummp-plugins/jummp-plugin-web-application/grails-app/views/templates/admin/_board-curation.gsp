<g:render template="/templates/admin/wcm-admin" plugin="jummp-plugin-web-application"/>
<g:render template="/templates/admin/curation-notes-management" plugin="jummp-plugin-web-application"/>
<g:render template="/templates/admin/model-classifier-configuration" plugin="jummp-plugin-web-application"/>
<g:render template="/templates/admin/mom-management" plugin="jummp-plugin-web-application"/>
<g:render template="/templates/admin/publication-management" plugin="jummp-plugin-web-application"/>
<g:render template="/templates/admin/redis-cache-management" plugin="jummp-plugin-web-application"/>
<g:render template="/templates/admin/tag-management" plugin="jummp-plugin-web-application"/>
