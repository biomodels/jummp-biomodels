<li class="accordion-item" data-accordion-item>
    <!-- Accordion tab title -->
    <a href="${createLink(controller: 'curationNotes')}" class="accordion-title">Curation Notes Management</a>
    <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
    <div class="accordion-content" data-tab-content>
        <p>Manage model curation notes</p>
        <a href="${createLink(controller: 'curationNotes')}">Access service</a>
    </div>
</li>
