<li class="accordion-item" data-accordion-item>
    <!-- Accordion tab title -->
    <a href="${createLink(controller: 'classifierConfigure')}" class="accordion-title">Model Classifier
    Configuration</a>
    <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
    <div class="accordion-content" data-tab-content>
        <p>Manage configurations of model classifier</p>
        <a href="${createLink(controller: 'classifierConfigure')}">Access service</a>
    </div>
</li>
