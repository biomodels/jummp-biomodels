<li class="accordion-item" data-accordion-item>
    <!-- Accordion tab title -->
    <a href="${createLink(controller: 'ModelOfTheMonth')}" class="accordion-title">Model Of The Month
    Entry</a>
    <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
    <div class="accordion-content" data-tab-content>
        <p>Manage Entries of Model Of The Month</p>
        <a href="${createLink(controller: 'ModelOfTheMonth')}">Access service</a>
    </div>
</li>
