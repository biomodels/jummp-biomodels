<li class="accordion-item" data-accordion-item>
    <!-- Accordion tab title -->
    <a href="${createLink(controller: 'omicsdi')}" class="accordion-title">OmicsDI Export Settings</a>
    <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
    <div class="accordion-content" data-tab-content>
        <p>Manage settings for OmicsDI Export</p>
        <a href="${createLink(controller: 'omicsdi')}">Access service</a>
    </div>
</li>
