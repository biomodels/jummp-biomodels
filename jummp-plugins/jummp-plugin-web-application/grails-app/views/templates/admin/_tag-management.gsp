<li class="accordion-item" data-accordion-item>
    <!-- Accordion tab title -->
    <a href="${createLink(controller: 'tag')}" class="accordion-title">Tag Management</a>
    <!-- Accordion tab content: it would start in the open state due to using the `is-active` state class. -->
    <div class="accordion-content" data-tab-content>
        <p>Manage model tags</p>
        <a href="${createLink(controller: 'tag')}">Access service</a>
    </div>
</li>
