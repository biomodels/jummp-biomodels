<h3 class="big-message-on-last-step"><strong>Model Created!</strong></h3>
<h4>Your model has been deposited successfully!</h4>
<img src="${grailsApplication.config.grails.serverURL}/images/biomodels/check-mark-success-submission.png"
     class="fit-image" style="width: 12%">
<p>
    It has been assigned perennial identifier <a href="${modelURL}" id="modelURL">${modelId}</a>.
</p>
<p>
    Thank you for submitting your model.
</p>
<p>Your model is private. To make it publicly available asap, please follow up
    <a href="https://www.ebi.ac.uk/biomodels/faq#access-after-submission"
       title="Access more information about publishing your submission">the instruction</a>.</p>
