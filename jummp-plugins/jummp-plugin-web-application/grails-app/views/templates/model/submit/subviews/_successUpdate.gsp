<h3 class="big-message-on-last-step"><strong><g:message code="submission.confirmation.update.header"/></strong></h3>
<p><g:message code="submission.confirmation.update.first.message" args="${[]}"/></p>
<p><g:message code="submission.confirmation.update.second.message"
              args="${[createLink(controller: "model", action:"show", id: modelId)]}"/></p>
<p>This revision is private. To make it publicly available asap, please consult
    <a href="https://www.ebi.ac.uk/biomodels/faq#access-after-submission"
       title="Access more information about publishing your submission">the procedure</a>.</p>
