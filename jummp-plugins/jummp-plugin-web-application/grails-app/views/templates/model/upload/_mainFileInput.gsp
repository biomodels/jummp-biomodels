<tr class="prop">
    <td class="value" style="width: 20%">
        <input type="file" id="mainFile" name="mainFile" class="mainFile"></td>
    <td class="name" style="width: 80%">
        <input type="text" id="mainFileDescription" name="mainFileDescription"
               placeholder="Please enter a description about the file type and format (e.g., SBML L2V4 representation of My Very Cool Model)"
               required></td>
</tr>
