<tr>
    <td><a href="${g.createLink(controller: "publication", action: "show",
        params: [id: publication.id])}">${publication.link}</a></td>
    <td><a href="${g.createLink(controller: "publication", action: "show",
        params: [id: publication.id])}">${publication.title}</a></td>
    <td>${publication.journal}</td>
    <td>${publication.affiliation}</td>
    <td>${publication.synopsis}</td>
    <td>${publication.year}</td>
    <td>${publication.month}</td>
    <td>${publication.day}</td>
    <td>${publication.volume}</td>
    <td>${publication.issue}</td>
    <td>${publication.pages}</td>
    <td>${publication.authors.join("; ")}</td>
</tr>
